#!/bin/sh

./clean.sh      # clean existing compiled files

set -e
. ./build.sh

mkdir -p isodir
mkdir -p isodir/boot
mkdir -p isodir/boot/grub

cp sysroot/boot/myos.kernel isodir/boot/myos.kernel
cat > isodir/boot/grub/grub.cfg << EOF
menuentry "djos" {
	multiboot /boot/myos.kernel
}
EOF
grub2/./grub-mkrescue -o myos.iso isodir
