# Operating System project (WIP)
## GRUB-2.06 + personal kernel implementation
Kernel is written with i686-elf (32-bit) platform in mind.  </br>
GRUB source code is included, as per its [GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.en.html), though the code itself isn't modified. You can also download it from [here](https://ftp.gnu.org/gnu/grub/).  </br>
To build the kernel, we need i686-elf-gcc cross compiler. To build that, use [following steps](https://wiki.osdev.org/GCC_Cross-Compiler). Make sure to run  </br>
>```~/gcc-x.y.z$ ./contrib/download_prerequisites```  </br>

before "make-ing" gcc. This is necessary for debugging in codeblocks.

## Progress so far
Project structure follows the [meaty skeleton](https://wiki.osdev.org/Meaty_Skeleton) (though it could be reorganized), from the great and amazing folk of [OSDev.org](https://wiki.osdev.org/Main_Page)!  </br>
OS can be interacted with only through console. It offers crud operations over files. Disk driver is implemented using ATA PIO communication. Filesystem is my own creation, it requires blank (unformatted disk) to work. Systemcalls for filesystem operations are implemented.
## BUILDING & RUNNING
Run </br>
`./run.sh` </br>
Make sure to have a disk .img in the same folder. If you are on linux, you can easily create on by running: `dd if=/dev/zero of=my_dis.img bs=1M count=4096`. 'count' parameter determines the size of the image. Adjust it to change size (this one is 4GB).

## DEBUGGING - How to use QEMU and CodeBlocks to debug kernel
Code structure changed a lot, so I will add this section later.

## Things left to do
I wish to refactor certain code segments. </br>  
Later on, I'll add dynamic memory allocation and memory management. 
