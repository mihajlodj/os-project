#include <string.h>

/*
*  Compare Strings without Case Sensitivity
*/
int strcasecmp(const char* str1, const char* str2) {
    size_t len1 = strlen(str1);
    size_t len2 = strlen(str2);
    if (len1 != len2) {
        return 0;  // false
    }
    char c1;
    char c2;
    for (size_t i = 0; i < len1; i++) {
        c1 = str1[i];
        c2 = str2[i];
        if (isletter(c1) && isletter(c2)) {
            c1 = ('A' <= c1 && c1 <= 'Z') ? c1 : (char) (c1 - 32);
            c2 = ('A' <= c2 && c2 <= 'Z') ? c2 : (char) (c2 - 32);
            if (c1 != c2) {
                return 0;
            }
        } else if (!isletter(c1) && !isletter(c2)) {
            if (c1 != c2) {
                return 0;
            }
        } else {
            return 0;
        }
    }
    return 1;
}
