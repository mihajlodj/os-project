#include <string.h>

/*
* The C library function char *strchr(const char *str, int c) searches for the first occurrence of the character c (an unsigned char)
* in the string pointed to by the argument str.
*/

char* strchr(const char *str, int c) {
    char* ret = NULL;
    while (*str != '\0') {
        if ((char)c == *str) {
            ret = str;
            break;
        }
        str++;
    }
    return ret;
}