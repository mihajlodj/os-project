#include <string.h>

int toupper(int ch) {
    if ('a' <= (char) ch && (char) ch <= 'z') {
        ch -= 32;
    }
    return ch;
}