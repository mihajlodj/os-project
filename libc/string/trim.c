#include <string.h>

void trim(char* str) { // remove trailing whitespaces
    size_t len = strlen(str);
	char* start = str;

    size_t textstart = 0;

    while (isspace(*start)) {
        textstart++;
        start++;
    }

    if (*start == '\0') { // reached the end of the string - there are only whitespaces.
        *str = '\0';
        return; 
    }

    size_t textend = len - 1;
    char* end = str + textend;
    while (isspace(*end)) {
        textend--;
        end--;
    }

    size_t textlength = textend - textstart + 1; // +1 to include \0
    memmove(str, start, textlength);
    *(str + textlength) = '\0';
}
