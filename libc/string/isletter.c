#include <string.h>

int isletter(char c) {
    if (('A' <= c && c <= 'Z') || ('a' <= c && c <= 'z')) {
        return 1;
    }
    return 0;
}