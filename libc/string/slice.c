#include <string.h>

// modifies the string so it only contains it's substring before transformation.
// index parameters are inclusive.
void slice(char* str, int idx1, int idx2) {
    if (idx1 == idx2) { // taking single character
        str[0] = str[idx1];
        str[1] = '\0';
        return;
    }

    size_t len = strlen(str);
    if (len < idx1 || len < idx2 || idx1 < 0 || idx2 < 0 || idx1 > idx2) { // error
        return;
    }
    
    memmove(str, str + idx1, idx2 - idx1);
    str[idx2 - idx1] = '\0';

}