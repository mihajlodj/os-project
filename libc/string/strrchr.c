#include <string.h>

/*
* The C library function char *strrchr(const char *str, int c) searches for the last occurrence of the character c (an unsigned char)
* in the string pointed to, by the argument str.
* 
* This function returns a pointer to the last occurrence of character in str. If the value is not found, the function returns a null pointer.
*/
char* strrchr(const char *str, int c) {
    char* ret = NULL;
    size_t len = strlen(str) - 1; // exclude '\0'
    for (int i = len; i >= 0; i--) {
        if (str[i] == (char)c) {
            ret = (char*) (str + i);
            break;
        }
    }
    return ret;
}