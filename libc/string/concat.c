#include <string.h>

// Copies str2 onto str1
// Function does copying instead of moving, to avoid making dangling references
void concat(char* str1, char* str2) {
    size_t len1 = strlen(str1);
    size_t len2 = strlen(str2);
    len2++; // include \0

    memcpy(str1+len1, str2, len2);
}