#include <string.h>

/*
*  Compare Strings without Case Sensitivity, but only first 'count' number of characters
*/
int strncasecmp(const char* str1, const char* str2, size_t count) {
    /*
    * If 'str1' is longer than 'str2', and 'count' is longer than 'str2' but shorter than 'str1',
    * function will compare only up to length of the shortest string (in this case, 'str2').
    * That way, I don't have to handle errors, but getting false positives becomes a thing.
    * This isn't a desired trait for this function, but I don't care. For what little I use it, it's fine.
    */
    size_t len1 = strlen(str1);
    size_t len2 = strlen(str2);
    size_t cnt1 = count;
    size_t cnt2 = count;

    // ensure no index error 
    if (cnt1 > len1) {cnt1 = len1;}
    if (cnt2 > len2) {cnt2 = len2;}
    if (cnt1 > cnt2) {count = cnt2;}
    else {count = cnt1;}

    char c1, c2;
    for (size_t i = 0; i < count; i++) {
        c1 = str1[i];
        c2 = str2[i];
        if (isletter(c1) && isletter(c2)) {
            c1 = ('A' <= c1 && c1 <= 'Z') ? c1 : (char) (c1 - 32);
            c2 = ('A' <= c2 && c2 <= 'Z') ? c2 : (char) (c2 - 32);
            if (c1 != c2) {
                return 0;
            }
        } else if (!isletter(c1) && !isletter(c2)) {
            if (c1 != c2) {
                return 0;
            }
        } else {
            return 0;
        }
    }
    return 1;
}