#include <string.h>

int isspace(char c) {
    char carriage = '\r';
    char formfeed = '\f';
	char space = ' ';
    char tab = '\t';
    char vtab = '\v';
    char nl = '\n';
    
    if (c == carriage || c == formfeed || c == space || c == tab || c == vtab || c == nl) {
        return 1;
    }
    return 0;
}
