#include <string.h>
 /*
 The function takes three arguments:

    dstptr: a pointer to the destination memory location where the data will be copied.
    srcptr: a pointer to the source memory location where the data will be read from.
    size: the number of bytes to copy.

The function first casts the dstptr and srcptr arguments to unsigned char pointers.
This is because unsigned char is the smallest addressable unit of memory, and using this type allows the function to copy data byte-by-byte.

The function then checks whether the destination memory region comes before or after the source memory region in memory.
If the destination comes before the source, the function uses a for loop to copy the data from the source to the destination byte-by-byte, 
	starting at the beginning of the memory region and moving towards the end.

If the destination comes after the source, the function uses a for loop to copy the data from the source to the destination byte-by-byte, 
	starting at the end of the memory region and moving towards the beginning. 
This is necessary to avoid overwriting data in the source memory region that has not yet been read.

Finally, the function returns a pointer to the destination memory location, which allows the caller to check whether the function completed successfully.
*/
void* memmove(void* dstptr, const void* srcptr, size_t size) {
	unsigned char* dst = (unsigned char*) dstptr;
	const unsigned char* src = (const unsigned char*) srcptr;
	if (dst < src) {
		for (size_t i = 0; i < size; i++)
			dst[i] = src[i];
	} else {
		for (size_t i = size; i != 0; i--)
			dst[i-1] = src[i-1];
	}
	return dstptr;
}