#include <string.h>
 
 /*
 This function compares the first size bytes of two memory areas pointed to by aptr and bptr.
 
 The function returns an integer less than, equal to, 
 	or greater than zero if the first size bytes of aptr are found to be respectively less than, equal to, or greater than the first size bytes of bptr.
 */
int memcmp(const void* aptr, const void* bptr, size_t size) {
	const unsigned char* a = (const unsigned char*) aptr;
	const unsigned char* b = (const unsigned char*) bptr;
	for (size_t i = 0; i < size; i++) {
		if (a[i] < b[i])
			return -1;
		else if (b[i] < a[i])
			return 1;
	}
	return 0;
}