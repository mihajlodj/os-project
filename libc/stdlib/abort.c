#include <stdio.h>
#include <stdlib.h>

volatile int system_state = 0; // 0 indicates normal operation, other values indicate error state

__attribute__((__noreturn__))
void abort(void) {
#if defined(__is_libk)
	// Kernel panic. Dissables interrupts and halts the processor to prevent damage to the system.
	/*
	* A kernel panic is a safety measure taken by an operating system's kernel upon detecting an internal fatal error
	* in which either it is unable to safely recover or continuing to run the system would have a higher risk of major data loss.
	*/
	printf("kernel: panic: abort()\n");
	asm("cli");  // Disable interrupts
	while (1) {
		asm("hlt"); // Halt the system
	}
#else
	// TODO: Abnormally terminate the process as if by SIGABRT.
	printf("abort()\n");
	system_state = 1;
#endif
	while (1) { }
	__builtin_unreachable();
}
