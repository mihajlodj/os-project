#ifndef _STRING_H
#define _STRING_H 1

#include <sys/cdefs.h>

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

int memcmp(const void*, const void*, size_t);
void* memcpy(void* __restrict, const void* __restrict, size_t);
void* memmove(void*, const void*, size_t);
void* memset(void*, int, size_t);
size_t strlen(const char*);
void trim(char*);
int isspace(char c);
void concat(char* str1, char* str2);
void slice(char* str, int idx1, int idx2);
int strcasecmp(const char* str1, const char* str2);
int strncasecmp(const char* str1, const char* str2, size_t count);
int isletter(char c);
char* strchr(const char *str, int c);
char* strrchr(const char *str, int c);
int toupper(int ch);

#ifdef __cplusplus
}
#endif

#endif
