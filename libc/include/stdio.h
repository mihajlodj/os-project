#ifndef _STDIO_H
#define _STDIO_H 1

#include <sys/cdefs.h>
#include <stddef.h>
#include <stdbool.h>

#define EOF (-1)

#ifdef __cplusplus
extern "C" {
#endif

int printf(const char* __restrict, ...);
int putchar(int);
int puts(const char*);
char* itoa(int i, char *strout, int base);
bool print(const char* data, size_t length);

#ifdef __cplusplus
}
#endif

#endif
