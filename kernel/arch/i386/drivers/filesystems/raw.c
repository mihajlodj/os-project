#include <kernel/raw.h>
#include <kernel/ATA_PIO.h>
#include <stdio.h>

bool is_block_empty(char block[]) {
    // block is empty if it doesn't have a filename (first 12 bytes are null)
    for (int i = 0; i < NAME_LIMIT + 1; i++) {
        if (block[i] != '\0') {
            return false;
        }
    }
    return true;
}
bool check_validity(uint8_t name_len, uint16_t contents_len) {
    if (name_len > NAME_LIMIT) {
        // printf("File name too long.\n");
        return false;
    } else if (name_len < 1) {
        // printf("File name can't be empty.\n");
        return false;
    }
    if (contents_len > SECTOR_SIZE - NAME_LIMIT - 1) {
        // printf("Content is too large.\n");
        return false;
    }
    return true;
}

static char block[512];
void clear_block() { 
    block[0] = '\0';
}

uint32_t find_free_LBA() {
    clear_block();
    uint32_t lba = LBA_START;
    do {
        if (ata_pio_read28(lba, 1, block) != OK) {
            return INVALID_LBA; 
        }
        lba++;
    } while (!is_block_empty(block));
    return lba - 1;
}

uint32_t raw_find_file(char* file_name) {
    uint8_t name_len = strlen(file_name);
    if (!check_validity(name_len, 1)) {
        return INVALID_LBA;
    }
    clear_block();
    uint32_t lba = LBA_START;
    while (memcmp(block, file_name, name_len) != 0) {
        ata_pio_read28(lba, 1, block);
        lba++;
        if (is_block_empty(block)) {
            // we passed the last block with no match
            return INVALID_LBA;
        }
    }
    lba--;
    return lba;
}
bool raw_load_file(uint32_t lba, char out[]) {
    return ata_pio_read28(lba, 1, out) == OK;
}
/*
* returns LBA of the created file;
*/
uint32_t raw_create_file(char* file_name, char contents[]) {
    uint8_t name_len = strlen(file_name);
    uint16_t contents_len = strlen(contents);

    if (!check_validity(name_len, contents_len)) {
        return INVALID_LBA;
    }
    if (raw_find_file(file_name) != INVALID_LBA) {
        return INVALID_LBA; // file with this name already exists.
    }
    char buffer[512];
    memcpy(buffer, file_name, name_len);
    char* buffer_ptr = buffer + 12;
    memcpy(buffer_ptr, contents, contents_len + 1); // include \0 terminator
    uint32_t lba = find_free_LBA();
    if (lba == INVALID_LBA) {
        printf("Something is wrong with the disk. Aborting.");
        return INVALID_LBA;
    }
    return ata_pio_write28(lba, 1, buffer) == OK ? lba : INVALID_LBA;
}
/*
* returns LBA of the read file
*/
uint32_t raw_read_file(char* file_name) {
    uint32_t lba = raw_find_file(file_name);
    if (lba == INVALID_LBA) { return INVALID_LBA; }
    clear_block();
    if (raw_load_file(lba, block)) {
        // print file
        char* block_ptr = block + 12;
        printf(block_ptr);
        printf("\n");
        return lba;
    }
    return INVALID_LBA;
}
/*
* returns LBA of the updated file
*/
uint32_t raw_update_file(char* file_name, char contents[]) {
    uint8_t name_len = strlen(file_name);
    uint16_t contents_len = strlen(contents);
    if (!check_validity(name_len, contents_len)) {
        return INVALID_LBA;
    }

    uint32_t lba = raw_find_file(file_name);
    if (lba == INVALID_LBA) return lba;

    clear_block();
    memcpy(block, file_name, name_len);
    char* buffer_ptr = block + 12;
    memcpy(buffer_ptr, contents, contents_len + 1); // include \0
    return ata_pio_write28(lba, 1, block) == OK ? lba : INVALID_LBA;
}
/*
* returns the first free LBA
*/
uint32_t raw_delete_file(char* file_name) {
    uint32_t lba_old = raw_find_file(file_name);
    if (lba_old == INVALID_LBA) return lba_old;

    uint32_t next_lba = lba_old + 1;

    clear_block();
    do {
        // load next sector
        if (ata_pio_read28(next_lba, 1, block) != OK) {
            return INVALID_LBA;
        }
        next_lba++;
        // write block into previous sector, effectively moving the memory
        if (ata_pio_write28(lba_old, 1, block) != OK) {
            return INVALID_LBA;
        }
        lba_old++;
    } while (!is_block_empty(block));
    return lba_old; // lba_old becomes first free linear address block
}

static char names[2048];

void raw_list_files() {
    clear_block();
    uint32_t lba = LBA_START;
    do {
        if (raw_load_file(lba, block)) {
            slice(block, 0, NAME_LIMIT+1);
            concat(names, block);
            concat(names, "    ");
            lba++;
        } else {
            break;
        }
        
    } while (!is_block_empty(block));

    printf(names);
    names[0] = '\0';
    printf("\n");
}