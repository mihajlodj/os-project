#include <kernel/util.h>
#include <kernel/ATA_PIO.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>

static char disk_info[2000];
// Control register defines
#define CONTROL 0x206
 
#define ALTERNATE_STATUS 0

// This code is to wait 400 ns
void ata_io_wait(const int p) {
	inb(PRIMARY_ALTSTAT_DCR);
	inb(PRIMARY_ALTSTAT_DCR);
	inb(PRIMARY_ALTSTAT_DCR);
	inb(PRIMARY_ALTSTAT_DCR);
}


/*
* REFER TO http://users.utcluj.ro/~baruch/media/siee/labor/ATA-Interface.pdf
* Chapter 7.8.2. Execute Device Diagnostic Command
*/
bool diagnose() {
    // DIAGNOSE MASTER
    uint8_t status = inb(PRIMARY_COMM_REGSTAT);

    check_status_for_bsy_drq(status);
    uint8_t original_value = inb(PRIMARY_DRIVE_HEAD);
    uint8_t bit_mask = ~(1 << 4); // Bit mask with only bit 4 (DEV bit) set to 0

    // Clear the DEV bit to 0 and leave other bits unchanged
    uint8_t cleared_value = original_value & bit_mask;
    outb(PRIMARY_DRIVE_HEAD, cleared_value); // Software clears the DEV bit of the Device register to 0, to diagnose master
    
    outb(PRIMARY_COMM_REGSTAT, ATA_CMD_DEVICE_DIAGNOSE); // No registers have to be initialized before writing the command code into the Command register.

    status = inb(PRIMARY_COMM_REGSTAT);
    check_status_for_bsy(status);  // abandon after 6 seconds

    disk_info_append("DISK DIAGNOSE");
    uint8_t results0 = inb(PRIMARY_ERR); // Diagnose results are found in ERR register.
    disk_info_append("\nDiagnostic code for master (1: ok, anything else is not ok): ");
    char value[20];
    itoa(results0, value, 10);
    disk_info_append(value);
    disk_info_append("\n");
    // determine signature (ATA or ATAPI)
    uint8_t sec_count = inb(PRIMARY_SECCOUNT);
    uint8_t lba_lo = inb(PRIMARY_LBA_LO);
    uint8_t lba_mid = inb(PRIMARY_LBA_MID);
    uint8_t lba_hi = inb(PRIMARY_LBA_HI);
    if (sec_count == 1 && lba_lo == 1 && lba_mid == 0 && lba_hi == 0) {
        disk_info_append("Device uses ATA signature.\n");
    } else if (sec_count == 1 && lba_lo == 1 && lba_mid == 0x14 && lba_hi == 0xEB) {
        disk_info_append("Device uses ATAPI signature.\n");
    } else {
        disk_info_append("Unknown signature.\n");
    }

    /* DIAGNOSE SLAVE
    *  I'm not sure if this is even supposed to be done. 
    *  And it doesn't work (returns 0). And I'm not sure if it's because of QEMU or something else.
    *  Thus I will implement ostrich algorithm and pretend that it's fine.
    */ 
    // original_value = inb(PRIMARY_DRIVE_HEAD);
    // bit_mask = (1 << 4);
    // cleared_value = original_value | bit_mask;
    // outb(PRIMARY_DRIVE_HEAD, cleared_value); // Software clears the DEV bit of the Device register to 1, to diagnose the slave
    // outb(PRIMARY_COMM_REGSTAT, ATA_CMD_DEVICE_DIAGNOSE);

    // status = inb(PRIMARY_COMM_REGSTAT);
    // check_status_for_bsy(status);  // abandon after 6 seconds

    // uint8_t results1 = inb(PRIMARY_ERR); // Diagnose results are found in ERR register.
    // disk_info_append("\nDiagnostic code for slave: %d\n\n", results1);
    // if (results0 == 1 && results1 == 1) {
    //     return 1;
    // }
    return results0 == 1;
}
/*
To use the IDENTIFY command, select a target drive by sending 0xA0 for the master drive, or 0xB0 for the slave, to the "drive select" IO port.
On the Primary bus, this would be port 0x1F6. Then set the Sectorcount, LBAlo, LBAmid, and LBAhi IO ports to 0 (port 0x1F2 to 0x1F5).
Then send the IDENTIFY command (0xEC) to the Command IO port (0x1F7). Then read the Status port (0x1F7) again.
If the value read is 0, the drive does not exist. For any other value: poll the Status port (0x1F7) until bit 7 (BSY, value = 0x80) clears.
Because of some ATAPI drives that do not follow spec, at this point you need to check the LBAmid and LBAhi ports (0x1F4 and 0x1F5) to see if they are non-zero.
If so, the drive is not ATA, and you should stop polling.
Otherwise, continue polling one of the Status ports until bit 3 (DRQ, value = 8) sets, or until bit 0 (ERR, value = 1) sets.

At that point, if ERR is clear, the data is ready to read from the Data port (0x1F0). Read 256 16-bit values, and store them.

Source: https://wiki.osdev.org/ATA_PIO_Mode#IDENTIFY_command
*/
bool identify() {
    if (!diagnose()) {
        disk_info_append("Disk diagnose failed. Exiting identification.\n");
        return false;
    }

    uint8_t status = inb(PRIMARY_COMM_REGSTAT);
    
    check_status_for_bsy_drq(status);

    outb(PRIMARY_DRIVE_HEAD, 0xA0);
    outb(PRIMARY_SECCOUNT, 0);
    outb(PRIMARY_LBA_LO, 0);
    outb(PRIMARY_LBA_MID, 0);
    outb(PRIMARY_LBA_HI, 0);
    outb(PRIMARY_COMM_REGSTAT, ATA_CMD_IDENTIFY); // identify command
    status = inb(PRIMARY_COMM_REGSTAT);
    ata_io_wait(PRIMARY_COMM_REGSTAT);
    
    if (status == 0) {
        disk_info_append("ATA bus does not exist.\n");
        return false;
    }

    uint8_t lba_mid;
    uint8_t lba_hi;
    while (status & ATA_SR_BSY) {
        lba_mid = inb(PRIMARY_LBA_MID);
        lba_hi = inb(PRIMARY_LBA_HI);
        if (lba_mid || lba_hi) {
            disk_info_append("It's not ATA :(\n");
            return false;
        }

        status = inb(PRIMARY_COMM_REGSTAT);
        ata_io_wait(PRIMARY_COMM_REGSTAT);
    }

    // Wait for ERR or DRQ
    while(!(status & (ATA_SR_ERR | ATA_SR_DRQ))) {
        status = inb(PRIMARY_COMM_REGSTAT);
        ata_io_wait(PRIMARY_COMM_REGSTAT);
    }
    
    if (status & ATA_SR_ERR) {
        printf("Error occured!\n");
        return false;
    }
    
    uint16_t data[256];
    insw(PRIMARY_DATA, data, (uint16_t) 256);
    
    add_device_information(data);
    outb(PRIMARY_COMM_REGSTAT, ATA_CMD_CACHE_FLUSH);

    return true;
}

int ata_pio_read28(uint32_t LBA, uint8_t sectorcount, void* target) {
    outb(PRIMARY_DRIVE_HEAD, 0xA0 | ((LBA >> 24) & 0x0F));
    outb(PRIMARY_SECCOUNT, sectorcount);
    outb(PRIMARY_LBA_LO, LBA & 0xFF);
    outb(PRIMARY_LBA_MID, (LBA >> 8) & 0xFF);
    outb(PRIMARY_LBA_HI, (LBA >> 16) & 0xFF);
    outb(PRIMARY_COMM_REGSTAT, ATA_CMD_READ_PIO);  // read command is 0x20
    uint8_t status = inb(PRIMARY_COMM_REGSTAT);
    
    check_status_for_bsy_err_drq(status);
    
    if (status & (ATA_SR_ERR | ATA_SR_DF)) {
        printf("Error occured!\n");
        return ERROR;
    }
    while (status & ATA_SR_BSY) {
        status = inb(PRIMARY_COMM_REGSTAT);
        ata_io_wait(PRIMARY_COMM_REGSTAT);
    }

    uint8_t i;
    for(i = 0; i < sectorcount; i++) {
        status = inb(PRIMARY_COMM_REGSTAT);
        while(!(status & ATA_SR_DRQ)) {
            status = inb(PRIMARY_COMM_REGSTAT);
            ata_io_wait(PRIMARY_COMM_REGSTAT);
        }
        
        insw(PRIMARY_DATA, (uint16_t* )target, (uint16_t)256);
        target += 256;
        
    }
    return OK;
}

int ata_pio_write28(uint32_t LBA, uint8_t sectorcount, void* source) {
    outb(PRIMARY_DRIVE_HEAD, 0xA0 | ((LBA >> 24) & 0x0F));
    outb(PRIMARY_SECCOUNT, sectorcount);
    outb(PRIMARY_LBA_LO, LBA & 0xFF);         // For commands that use 28-bit LBA addressing, the LBA Low register should be written with bits 7..0 of the LBA address.
    outb(PRIMARY_LBA_MID, (LBA >> 8) & 0xFF); // For commands that use 28-bit LBA addressing, the LBA Mid register should be written with bits 15..8 of the LBA address.
    outb(PRIMARY_LBA_HI, (LBA >> 16) & 0xFF); // For commands that use 28-bit LBA addressing, the LBA High register should be written with bits 23..16 of the LBA address.
    outb(PRIMARY_COMM_REGSTAT, ATA_CMD_WRITE_PIO);  // write command is 0x30
    
    uint8_t status = inb(PRIMARY_COMM_REGSTAT);
    check_status_for_bsy(status);

    if (status & (ATA_SR_ERR | ATA_SR_DF)) {
        printf("Error occured!\n");
        return ERROR;
    }
    uint8_t i;
    for(i = 0; i < sectorcount; i++) {
        // polling
        status = inb(PRIMARY_COMM_REGSTAT);
        while(!(status & ATA_SR_DRQ)) {
            status = inb(PRIMARY_COMM_REGSTAT);
        }
        outsw(PRIMARY_DATA, (uint16_t* )source, (uint16_t)256);
        source += 256;
        status = inb(PRIMARY_COMM_REGSTAT);
        if (status & (ATA_SR_DF | ATA_SR_ERR)) {
            printf("Drive write fault\n");
            // Flush the cache.
            outb(PRIMARY_COMM_REGSTAT, ATA_CMD_CACHE_FLUSH);
            return DRIVE_FAULT;
        }
    }
    
    // Flush the cache.
    outb(PRIMARY_COMM_REGSTAT, ATA_CMD_CACHE_FLUSH);
    // Poll for BSY.
    while(inb(PRIMARY_COMM_REGSTAT) & ATA_SR_BSY) {}
    return OK;
}

void check_status_for_bsy_err_drq(uint8_t status) {
    while (status & ATA_SR_BSY) {
        status = inb(PRIMARY_COMM_REGSTAT);
        ata_io_wait(PRIMARY_COMM_REGSTAT);
    }
    
    // Wait for ERR or DRQ
    while(!(status & (ATA_SR_ERR | ATA_SR_DRQ))) {
        status = inb(PRIMARY_COMM_REGSTAT);
        ata_io_wait(PRIMARY_COMM_REGSTAT);
    }
}

void check_status_for_bsy_drq(uint8_t status) {
     // Wait for BSY or DRQ
    while(status & (ATA_SR_BSY | ATA_SR_DRQ)) {
        status = inb(PRIMARY_COMM_REGSTAT);
        ata_io_wait(PRIMARY_COMM_REGSTAT);
    }
}

void check_status_for_bsy(uint8_t status) {
     // Wait for BSY
    while(status & ATA_SR_BSY) {
        status = inb(PRIMARY_COMM_REGSTAT);
        ata_io_wait(PRIMARY_COMM_REGSTAT);
    }
}

/*
* REFER TO http://users.utcluj.ro/~baruch/media/siee/labor/ATA-Interface.pdf
* Table 7.12 Meaning of words returned by the Identify Device command.
* Chapter 7.8.3 Identify Device Command
*/
void add_device_information(uint16_t data[]) {
    disk_info_append("\nDISK INFORMATION\n");

    char value[20];

    // CHS
    disk_info_append("Number of logical cylinders in the default CHS translation: ");
    itoa(data[1], value, 10);
    disk_info_append(value);
    disk_info_append("\n");
    disk_info_append("Number of logical heads in the default CHS translation: ");
    itoa(data[3], value, 10);
    disk_info_append(value);
    disk_info_append("\n");
    disk_info_append("Number of logical sectors per track in the default CHS translation: ");
    itoa(data[6], value, 10);
    disk_info_append(value);
    disk_info_append("\n");

    // SERIAL NUMBER
    char serial_number[21];
    identify_ascii_data(data, 10, 19, serial_number);
    disk_info_append("Serial number: ");
    trim(serial_number);
    concat(serial_number, "\n");
    disk_info_append(serial_number);

    // FIRMWARE REVISION
    char firmware[9];
    identify_ascii_data(data, 23, 26, firmware);
    disk_info_append("Firmware revision: ");
    trim(firmware);
    concat(firmware, "\n");
    disk_info_append(firmware);

    // PRINT MODEL NUMBER
    char model_number[41];
    identify_ascii_data(data, 27, 46, model_number);
    disk_info_append("Model number: ");
    trim(model_number);
    concat(model_number, "\n");
    disk_info_append(model_number);

    // CAPACITY IN CHS TRANSLATION
    disk_info_append("Capacity in sectors in the current CHS translation ");
    itoa(((uint32_t)data[58] << 16) | data[57], value, 10);
    disk_info_append(value);
    disk_info_append("\n");

    // LBA
    uint32_t total_addressable_sectors = ((uint32_t)data[61] << 16) | data[60];
    if (total_addressable_sectors > 0) {
        itoa(total_addressable_sectors, value, 10);
        disk_info_append("Total number of addressable sectors (28-bit LBA addressing): ");
        disk_info_append(value);
        disk_info_append("\n");
    } else {
        disk_info_append("LBA28 not supported!\n");
    }

    /* Refer to: http://1-2-8.net/mwva/karas/111-1C.PDF
    *  This is according to Seagate specification. The other document doesn't mention these bits.
    *  I don't know exactly what QEMU uses, though.
    */ 
    disk_info_append("Supports LBA (1: yes, 0: no): ");
    itoa(((data[49] & BIT_9) >> 9), value, 10);
    disk_info_append(value);
    disk_info_append("\n");

    disk_info_append("Supports DMA (1: yes, 0: no): ");
    itoa(((data[49] & BIT_8) >> 8), value, 10);
    disk_info_append(value);
    disk_info_append("\n");
    disk_info_append("\n");
}

void identify_ascii_data(uint16_t data[], uint16_t start_idx, uint16_t end_idx, char* ascii_sequence) {

    uint16_t step = 0;
    for (int i = start_idx; i <= end_idx; i++) {
        char first_char  = (char)((data[i] & (uint16_t) 0xFF00) >> 8);
        char second_char = (char)(data[i] & (uint16_t) 0x00FF);
        ascii_sequence[step * 2] = first_char;
        ascii_sequence[step * 2 + 1] = second_char;
        step++;
    }
    ascii_sequence[step*2] = '\0';
}

void disk_info_append(char* info) {
    concat(disk_info, info);
}

void print_disk_info() {
    printf(disk_info);
}