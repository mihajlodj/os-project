/*
* Command structure: command ["]arg1["] ["]arg2["]
* Square paranthesis mean that quotations is optional.
* The reason being that between quotations space (' ') is being ignored, whereas without them, THE FIRST space separates arg1 and arg2.
*
* Commands that take arguments (and invoke system calls) are CRUD commands over the filesystem.
* Read and Delete commands ignore (don't take) second argument.
* Update command HAS to have second argument
* Create command works with and without second argument
* 
* Commands that don't take arguments ("help" and "disk") just print stuff out to the terminal, because I like meaningless flavourtext.
*/

// TODO: make it more flexible and scalable in the future.

#include <kernel/cmd_parser.h>
#include <kernel/raw.h>
#include <kernel/ATA_PIO.h>

static COMMAND cmd = {
    .command = "",
    .arg1 = "",
    .arg2 = ""
};

// first word in command line buffer is always command.
// this function puts that word in COMMAND struct
// returns true for success. false for failure
bool extract_command(char* buffer) {
    size_t len = strlen(buffer);
    bool success = true;
    size_t i = 0;
    while (i < len) {
        if (buffer[i] == ' ') {
            break;
        } else if (i > 9) {
            success = false;
            break;
        } else if (buffer[i] == '\0') {
            break;
        } else {
            cmd.command[i] = buffer[i];
        }
        i++;
    }
    cmd.command[i] = '\0';
    return success;
}

void clear_cmd() {
    cmd.command[0] = '\0';
    cmd.arg1[0] = '\0';
    cmd.arg2[0] = '\0';
}

void execute_command(char* buffer) {
    // determine command
    if (strcasecmp(cmd.command, CMD_CREATE)) {
        if (fill_args(buffer)) {
            make_syscall(SYS_CREATE_FILE, &cmd);
        } else {
            printf("Command 'create' is missing an argument.\n");
        }
    } else if (strcasecmp(cmd.command, CMD_READ)) {
        if (fill_args(buffer)) {
            make_syscall(SYS_READ_FILE, &cmd);
        } else {
            printf("Command 'read' is missing an argument.\n");
        }
    } else if (strcasecmp(cmd.command, CMD_UPDATE)) {
        if (fill_args(buffer)) {
            make_syscall(SYS_WRITE_FILE, &cmd);
        } else {
            printf("Command 'update' is missing an argument.\n");
        }
    } else if (strcasecmp(cmd.command, CMD_DELETE)) {
        if (fill_args(buffer)) {
            make_syscall(SYS_DELETE_FILE, &cmd);
        } else {
            printf("Command 'delete' is missing an argument.\n");
        }
    } else if (strcasecmp(cmd.command, CMD_LIST)) {
        make_syscall(SYS_LIST_FILES, &cmd);
    } else if (strcasecmp(cmd.command, CMD_DISK)) {
        print_disk_info();
    } else if (strcasecmp(cmd.command, CMD_HELP)) {
        printf("Welcome to DJOS. Here's the list of available commands:\n");
        printf("-'disk' command takes no arguments and prints out diagnose results and device information.\n");
        printf("-'create' command takes 1 or two arguments, separated by ' ' and creates a file onto the file system (if disk has been identified)\n");
        printf("    eg: > create filename [file contents]\n");
        printf("    Note: argument inside of [] is optional\n");
        printf("-'list' command takes no arguments and prints all the file names on the disk (if disk has been identified)\n");
        printf("-'update' command takes two arguments and changes the file contents to newly typed text (if disk has been identified)\n");
        printf("    eg: > update filename file contents\n");
        printf("    Note: spaces in the second argument are allowed (only first two spaces count as argument separators)\n");
        printf("-'delete' command takes 1 argument and deletes the file from disk, if it exists (if disk has been identified)\n");
        printf("-'read' command takes 1 argument and prints the file contents in the terminal, if it exists (if disk has been identified)\n");
    } else {
        printf("Unknown command %s\n", cmd.command);
    }
}

bool fill_args(char* buffer) {
    // go past the command name
    char* arg1_start = strchr(buffer, ' ');
    trim(arg1_start);
    if (arg1_start == NULL) {
        clear_cmd();
        return false;
    }
    bool success1 = parse_arg1(arg1_start);
    // go past the first argument
    char* arg2_start = strchr(arg1_start, ' ');
    if (arg2_start == NULL && strcasecmp(cmd.command, CMD_UPDATE)) {
        clear_cmd();
        return false;
    }
    bool success2 = parse_arg2(arg2_start);
    return success1 && success2;
}

bool parse_arg1(char* arg1_start) {
    int idx = 0;
    char* iter = arg1_start;
    if ((*arg1_start) == '"') {
        iter++;
        while ((*iter) != '"') {
            if ((*iter) == '\0' || idx >= NAME_LIMIT - 1) {
                printf("First argument (filename) can't be over 11 characters.\n");
                return false;
            }
            cmd.arg1[idx] = (*iter);
            idx++;
            iter++;
        }
    }
    else {
        while ((*iter) != ' ' && (*iter) != '\0') {
            if (idx >= NAME_LIMIT - 1) {
                printf("First argument (filename) can't be over 11 characters.\n");
                return false;
            }
            cmd.arg1[idx] = (*iter);
            idx++;
            iter++;
        }
    }
    cmd.arg1[idx] = '\0';
    return true;
}
bool parse_arg2(char* arg2_start) {
    int idx = 0;
    char* iter = arg2_start + 1;
    if ((*arg2_start) == '"') {
        while ((*iter) != '"') {
            if ((*iter) == '\0' || idx >= ARG_LIMIT - 1) {
                printf("Invalid argument format.\n");
                return false;
            }
            cmd.arg2[idx] = (*iter);
            idx++;
            iter++;
        }
    }
    else {
        while ((*iter) != '\0') {
            if (idx >= ARG_LIMIT - 1) {
                printf("Invalid argument format.\n");
                return false;
            }
            cmd.arg2[idx] = (*iter);
            idx++;
            iter++;
        }
    }
    cmd.arg2[idx] = '\0';
    return true;
}

// interface

void parse_command(char* buffer) {
    clear_cmd();
    trim(buffer);
    if (extract_command(buffer)) {
        execute_command(buffer);

    } else {
        printf("Invalid input\n");
        clear_cmd();
    }
    
}