#include <kernel/util.h>
#include <kernel/keyboard.h>
#include <kernel/isr.h>
#include <kernel/tty.h>
#include <string.h>
#include <stdbool.h>
#include <kernel/cmd_parser.h>

static char key_buffer[512];
static bool caps = false;

#define SC_MAX 58

const char *sc_name[] = {"ERROR", "Esc", "1", "2", "3", "4", "5", "6",
                         "7", "8", "9", "0", "-", "=", "Backspace", "Tab", "Q", "W", "E",
                         "R", "T", "Y", "U", "I", "O", "P", "[", "]", "Enter", "Lctrl",
                         "A", "S", "D", "F", "G", "H", "J", "K", "L", ";", "'", "`",
                         "LShift", "\\", "Z", "X", "C", "V", "B", "N", "M", ",", ".",
                         "/", "RShift", "Keypad *", "LAlt", "Spacebar"};
const char sc_ascii[] = {'?', '?', '1', '2', '3', '4', '5', '6',
                         '7', '8', '9', '0', '-', '=', '?', '?', 'Q', 'W', 'E', 'R', 'T', 'Y',
                         'U', 'I', 'O', 'P', '[', ']', '?', '?', 'A', 'S', 'D', 'F', 'G',
                         'H', 'J', 'K', 'L', ';', '\'', '`', '?', '\\', 'Z', 'X', 'C', 'V',
                         'B', 'N', 'M', ',', '.', '/', '?', '?', '?', ' '};

static void keyboard_callback(registers_t *regs) {
    uint8_t scancode = inb(KEYBOARD_PORT);
    if (scancode > SC_MAX) return;
    if (scancode == KEY_CAPS_LOCK) {
        switch_caps();
    } else if (scancode == KEY_BACKSPACE) {
        if (do_backspace()) {
            terminal_backspace();
        }
    } else if (scancode == KEY_ENTER) {
        terminal_write("\n", 1);
        parse_command(key_buffer);
        clear_buffer();
        terminal_writestring("> ");
    } else if (strlen(key_buffer) >= 256) {
        // skip
    } else if (scancode == KEY_TAB && strlen(key_buffer) < 252) {
        terminal_writestring("    ");
        concat(key_buffer, "    ");
    } else {
        char uc = sc_ascii[(int) scancode];
        if (uc >= 'A' && uc <= 'Z' && !caps) {
            uc = uc + 32;
        }
        char str[2] = {uc, '\0'};
        concat(key_buffer, str);
        terminal_writestring(str);
    }
}

void clear_buffer() {
    for (int i = 0; i < 256; i++) {
        key_buffer[i] = '\0';
    }
}

int do_backspace() {
    size_t buffer_len = strlen(key_buffer);
    if (buffer_len == 1) { key_buffer[0] = '\0'; return 1; }
    else if (buffer_len > 1) {
        slice(key_buffer, 0, buffer_len - 1);
        return 1;
    }
    return 0;
}

void switch_caps() {
	if (caps == true) {
		caps = false;
	} else {
		caps = true;
	}
}


void init_keyboard() {
    register_interrupt_handler(IRQ1, keyboard_callback);
}