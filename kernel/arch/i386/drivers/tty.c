#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include <kernel/tty.h>
#include <kernel/cursor.h>


#include "vga.h"

#define SCREEN (uint16_t*)0xB8000

static uint16_t* const VGA_MEMORY = (uint16_t*) 0xB8000;

static size_t terminal_row;
static size_t terminal_column;
static uint8_t terminal_color;
static uint16_t* terminal_buffer;

void terminal_initialize(void) {
	terminal_row = 0;
	terminal_column = 0;
	terminal_color = vga_entry_color(VGA_COLOR_LIGHT_GREY, VGA_COLOR_BLACK);
	terminal_buffer = VGA_MEMORY;
	for (size_t y = 0; y < VGA_HEIGHT; y++) {
		for (size_t x = 0; x < VGA_WIDTH; x++) {
			const size_t index = y * VGA_WIDTH + x;
			terminal_buffer[index] = vga_entry(' ', terminal_color);
		}
	}
	enable_cursor(0, 0);
}

void terminal_setcolor(uint8_t color) {
	terminal_color = color;
}

void terminal_putentryat(unsigned char c, uint8_t color, size_t x, size_t y) {
	const size_t index = y * VGA_WIDTH + x;
	terminal_buffer[index] = vga_entry(c, color);
}

void terminal_scroll(int line) {
	memcpy(SCREEN + (line - 1) * VGA_WIDTH, SCREEN + line * VGA_WIDTH, VGA_WIDTH * sizeof(uint16_t));
}
 
void terminal_delete_last_line() {
	int *ptr;
    long unsigned int x;
	for(x = 0; x < VGA_WIDTH * 2; x++) {
		ptr = (int *) (0xB8000 + (VGA_WIDTH * 2) * (VGA_HEIGHT - 1) + x);
		*ptr = 0;
	}
}
 
void terminal_putchar(char c) {
	int line;
	unsigned char uc = c;
	if (c == '\n') {
        terminal_column = 0;
        terminal_row++;
    } else {
		terminal_putentryat(uc, terminal_color, terminal_column, terminal_row);
		if (++terminal_column == VGA_WIDTH) {
			terminal_column = 0;
			terminal_row++;
		}
	}

	if (terminal_row == VGA_HEIGHT)
	{
		for(line = 1; line <= VGA_HEIGHT - 1; line++)
		{
			terminal_scroll(line);
		}
		terminal_delete_last_line();
		terminal_row = VGA_HEIGHT - 1;
	}

	update_cursor(terminal_column, terminal_row);
}

void terminal_write(const char* data, size_t size) {
	for (size_t i = 0; i < size; i++)
		terminal_putchar(data[i]);
}

void terminal_writestring(const char* data) {
	terminal_write(data, strlen(data));
}

void terminal_backspace() {
	if (terminal_column == 0 && terminal_row == 0) return; // ignore backspace at start of the matrix
	if (terminal_column > 0) {
		terminal_column--;
		terminal_putentryat(' ', terminal_color, terminal_column, terminal_row); // consume character
		update_cursor(terminal_column, terminal_row);
	} else if (terminal_column == 0 && terminal_row > 0) {
		terminal_row--;
		terminal_column = VGA_WIDTH;
		terminal_putentryat(' ', terminal_color, terminal_column, terminal_row); // consume character
		update_cursor(terminal_column, terminal_row);
	}
}
