#include <kernel/syscalls.h>
#include <kernel/raw.h>
#include <stdio.h>
void (*syscall_table[])() = {
    [SYS_CREATE_FILE] = sys_create_file,
    [SYS_DELETE_FILE] = sys_delete_file,
    [SYS_READ_FILE]   = sys_read_file,
    [SYS_WRITE_FILE]  = sys_write_file,
    [SYS_LIST_FILES]  = sys_list_files
};


void sys_create_file(registers_t* regs) {
    COMMAND* cmd = (COMMAND*)regs->ebx;
    char* filename = cmd->arg1;
    char* contents = cmd->arg2;
    if (raw_create_file(filename, contents) == INVALID_LBA) {
        printf("File creation failed\n");
    }
}

void sys_read_file(registers_t* regs) {
    COMMAND* cmd = (COMMAND*)regs->ebx;
    char* filename = cmd->arg1;
    raw_read_file(filename);
}

void sys_write_file(registers_t* regs) {
    COMMAND* cmd = (COMMAND*)regs->ebx;
    char* filename = cmd->arg1;
    char* contents = cmd->arg2;
    raw_update_file(filename, contents);
}

void sys_delete_file(registers_t* regs) {
    COMMAND* cmd = (COMMAND*)regs->ebx;
    char* filename = cmd->arg1;
    raw_delete_file(filename);
}

void sys_list_files(registers_t* regs) {
    raw_list_files();
}


void make_syscall(int CMD_ID, COMMAND* command) {
    // when more syscalls get added, this needs to change.
    if (FS_ENABLED) {
        asm volatile (
            "int $0x80"
            : 
            : "a"(CMD_ID), "b"(command)
        );        
    } else {
        printf("No working disk. This operation is disabled.\n");
    }

}

void enable_FS_syscalls() {
    FS_ENABLED = true;
}