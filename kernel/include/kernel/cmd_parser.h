#pragma once

#include <kernel/syscalls.h>

#include <string.h>
#include <stdbool.h>
#include <stdio.h>

#define CMD_CREATE "create"
#define CMD_READ "read"
#define CMD_UPDATE "update"
#define CMD_DELETE "delete"
#define CMD_DISK "disk"
#define CMD_HELP "help"
#define CMD_LIST "list"

#define CMD_NAME_LENGTH 9
#define NAME_LIMIT 11
#define ARG_LIMIT 500

typedef struct cmd {
    char command[CMD_NAME_LENGTH + 1]; // +1 for null terminator
    char arg1[NAME_LIMIT + 1];
    char arg2[ARG_LIMIT];
} __attribute__((packed)) COMMAND;

// Forward declaration of the make_syscall function
void make_syscall(int CMD_ID, struct cmd* command);

void parse_command(char* buffer);
void clear_cmd();
bool fill_args(char* buffer);
bool extract_command(char* buffer);
void execute_command(char* buffer);
bool parse_arg1(char* arg1_start);
bool parse_arg2(char* arg2_start);
