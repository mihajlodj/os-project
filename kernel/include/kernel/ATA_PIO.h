/*
* ATA PIO Mode
* Interface standard for communicating with hard drive, where CPU takes brunt of the work.
* It's fine for single-threaded (singletasking) systems,
* But not good enough for multitasking systems (due to CPU usage).
*/

#include <stdint.h>
#include <stdbool.h>

// REGISTERS
#define PRIMARY_DATA         0x1F0
#define PRIMARY_ERR          0x1F1
#define PRIMARY_SECCOUNT     0x1F2
#define PRIMARY_LBA_LO       0x1F3
#define PRIMARY_LBA_MID      0x1F4
#define PRIMARY_LBA_HI       0x1F5
#define PRIMARY_DRIVE_HEAD   0x1F6 // also known as device register
#define PRIMARY_COMM_REGSTAT 0x1F7
#define PRIMARY_ALTSTAT_DCR  0x376 // DCR - device control register
#define PRIMARY_CONTROL_DCR  0x3F6

// STATUS
#define ATA_SR_BSY     0x80    // Busy
#define ATA_SR_DRDY    0x40    // Drive ready
#define ATA_SR_DF      0x20    // Drive write fault
#define ATA_SR_DSC     0x10    // Drive seek complete
#define ATA_SR_DRQ     0x08    // Data request ready
#define ATA_SR_CORR    0x04    // Corrected data
#define ATA_SR_IDX     0x02    // Index
#define ATA_SR_ERR     0x01    // Error

// COMMAND
#define ATA_CMD_READ_PIO          0x20
#define ATA_CMD_READ_PIO_EXT      0x24
#define ATA_CMD_READ_DMA          0xC8
#define ATA_CMD_READ_DMA_EXT      0x25
#define ATA_CMD_WRITE_PIO         0x30
#define ATA_CMD_WRITE_PIO_EXT     0x34
#define ATA_CMD_WRITE_DMA         0xCA
#define ATA_CMD_WRITE_DMA_EXT     0x35
#define ATA_CMD_CACHE_FLUSH       0xE7
#define ATA_CMD_CACHE_FLUSH_EXT   0xEA
#define ATA_CMD_PACKET            0xA0
#define ATA_CMD_IDENTIFY_PACKET   0xA1
#define ATA_CMD_IDENTIFY          0xEC
#define ATA_CMD_DEVICE_DIAGNOSE   0X90

#define BIT_9 (1 << 9)
#define BIT_8 (1 << 8)
#define ERROR 0
#define DRIVE_FAULT -1
#define OK 1

void ata_io_wait(const int p);

bool diagnose();
bool identify();
int ata_pio_read28(uint32_t LBA, uint8_t sectorcount, void* target);
int ata_pio_write28(uint32_t LBA, uint8_t sectorcount, void* source);
void ata_pio_read48(uint64_t LBA, uint16_t sectorcount, uint8_t* target);
void ata_pio_write48(uint64_t LBA, uint16_t sectorcount, uint8_t* source);

void add_device_information(uint16_t data[]);
void identify_ascii_data(uint16_t data[], uint16_t start_idx, uint16_t end_idx, char* ascii_sequence);

void check_status_for_bsy(uint8_t status);
void check_status_for_bsy_drq(uint8_t status);
void check_status_for_bsy_err_drq(uint8_t status);

void print_disk_info();