#pragma once

#include <kernel/cmd_parser.h>
#include <kernel/isr.h>
#include <stdbool.h>

// Forward declaration of the COMMAND struct
struct cmd;
typedef struct cmd COMMAND;

#define SYS_CREATE_FILE 1
#define SYS_DELETE_FILE 2
#define SYS_READ_FILE   3
#define SYS_WRITE_FILE  4
#define SYS_LIST_FILES  5

static bool FS_ENABLED = false;

void enable_FS_syscalls();

void sys_create_file(registers_t* regs);
void sys_read_file(registers_t* regs);
void sys_write_file(registers_t* regs);
void sys_delete_file(registers_t* regs);
void sys_list_files(registers_t* regs);

extern void (*syscall_table[])();

void make_syscall(int CMD_ID, COMMAND* command);

