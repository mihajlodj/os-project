#pragma once
#include <stdint.h>
#include <stddef.h>


static inline void delay(uint8_t cycles) {
    for (uint8_t i = 0; i < cycles; i++) {
        __asm__ volatile("nop");
    }
}

static inline void outb(uint16_t port, uint8_t value)
{
    __asm__ volatile("outb %%al,%%dx": :"d" (port), "a" (value));
}

static inline unsigned char inb(unsigned int port)
{
   unsigned char ret;
   __asm__ volatile("inb %%dx,%%al":"=a" (ret):"d" (port));
   return ret;
}

static inline void outw(uint16_t port, uint16_t value)
{
    __asm__ volatile("outw %%ax,%%dx": :"d" (port), "a" (value));
}

static inline uint16_t inw(unsigned int port)
{
   uint16_t ret;
   __asm__ volatile("inw %%dx,%%ax":"=a" (ret):"d" (port));
   return ret;
}

static inline void insw(uint16_t port, void* addr, uint16_t size) {
    uint16_t *ptr = (uint16_t *)addr;
    unsigned int i;

    for (i = 0; i < size; i++) {
        ptr[i] = inw(port);
    }
}

// static inline void outsw(uint16_t port, void *addr, uint8_t size) {
//     uint16_t *ptr = (uint16_t *)addr;
//     unsigned int i;

//     for (i = 0; i < size; i++) {
//         outw(port, ptr[i]);
//         delay(15);
//     }
// }


static inline void outsw(uint16_t __port, const void *__buf, unsigned long __n) {
	__asm__ volatile("cld; rep; outsw"
			: "+S"(__buf), "+c"(__n)
			: "d"(__port));
}