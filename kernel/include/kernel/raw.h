// basic block storage, lol

#pragma once

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#define SECTOR_SIZE 512
#define NAME_LIMIT 11
#define LBA_START 1   // LBA starts from 1
#define INVALID_LBA 0 // 0 is invalid value for LBA

// not used anywhere, but it helps visualize what 'file' consists of.
typedef struct file {
    uint32_t lba_position;
    char name[NAME_LIMIT + 1]; // text + null terminator
    char contents[SECTOR_SIZE - NAME_LIMIT - 1];
} __attribute__((packed)) FILE;

bool raw_init();
uint32_t raw_create_file(char* file_name, char contents[]);
uint32_t raw_read_file(char* file_name);
uint32_t raw_update_file(char* file_name, char contents[]);
uint32_t raw_delete_file(char* file_name);
void raw_list_files();