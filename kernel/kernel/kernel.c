#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <kernel/idt.h>
#include <kernel/isr.h>
#include <kernel/keyboard.h>
#include <kernel/tty.h>
#include <stdint.h>
#include <kernel/syscalls.h>
#include <kernel/ATA_PIO.h>

void kernel_main(void) {
	terminal_initialize();
	
	isr_install();

    asm volatile("sti");

    init_keyboard();
	printf("                                WELCOME TO DJOS!\n\n");
	printf("To see the details about available commands and how to use this OS, type 'help'\n");
	if (identify()) {
		enable_FS_syscalls();
	} else {
		printf("Could not identify disk. Filesystem operation are disabled.\n");
	}

	printf("> ");
	while (1) {

	}
	
	printf("exiting !\n");
}
