#!/bin/sh

set -e
. ./iso.sh
sync
# dd if=/dev/zero of=my_disk.img bs=1M count=4096
# mkfs.vfat -F 32 -S 512 -n "DJOS" my_disk.img >/dev/null
# mmd -i my_disk.img "::boot"
# mcopy -i my_disk.img isodir/boot/myos.kernel "::boot/"
# mcopy -i my_disk.img isodir/boot/grub/grub.cfg "::"
# dd if=myos.iso of=my_disk.img status=progress

# run
# qemu-system-i386 -drive id=disk,file=my_disk.img,if=none,format=$FMT -device ahci,id=ahci -device ide-hd,drive=disk,bus=ahci.0 -cdrom myos.iso -boot d
qemu-system-i386 -hda my_disk.img -cdrom myos.iso -boot d
# convert
# qemu-img convert -f raw -O qcow2 my_disk.img my_disk.qcow2
